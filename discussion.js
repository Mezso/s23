//CRUD
//Create, read, Update, and Delete

db.users.insertOne({
	"firstName" : "Tony",
	"lastName": "Stark",
	"username": "iAmIronMan",
	"email":"iloveyou3000@mail.com",
	"password":"starkIndustries",
	"isAdmin":true
})

db.users.insertMany([
{
		"firstName" : "Pepper",
	"lastName": "pots",
	"username": "rescueArmor",
	"email":"pepper@mail.com",
	"password":"whereistony",
	"isAdmin":false
},
{
		"firstName" : "steve",
	"lastName": "rogers",
	"username": "thecaptain",
	"email":"captAmerica@mail.com",
	"password":"iCanlifeMjolnirToo",
	"isAdmin":false
},
{
		"firstName" : "Thor",
	"lastName": "odison",
	"username": "mightyThor",
	"email":"thornotloki@mail.com",
	"password":"IamworthyToo",
	"isAdmin":false
},
{
		"firstName" : "Loki",
	"lastName": "odison",
	"username": "GodOfMischief",
	"email":"loki@mail.com",
	"password":"iamreallyloki",
	"isAdmin":false
}
])

db.users.insertMany([
{
	"name":"javascript",
	"price",3500,
	"description":"Learn javascript in a week!",
	"isActive":true
},
{
	"name":"HTML",
	"price",1000,
	"description":"Learn Basic HTML in 3days!",
	"isActive":true
},
{
	"name":"CSS",
	"price",2000,
	"description":"Make you website fancy, learnCSS NOW!",
	"isActive":true
}

])

db.users.find()

db.collections.findOne({"isAdmin":true})

db.users.find({"lastName":"odison","firstName":"Loki"})

db.users.updateOne({"lastName":"pots"},{$set: {"lastName":"Stark"}})

db.users.updateMany({"lastName":"odison"},{$set:{"isAdmin":true}})

db.users.updateOne({},{$set:{"email":"starindustries@mail.com"}})

db.courses.updateMany({},{$set:{"enrollees":10}})

db.courses.updateMany({},{$set:{"enrollees":10}})

db.users.deleteOne({"isAdmin":false})

db.users.deleteMany({"lastName":"odison"})

db.users.deleteOne({})

db.users.deleteMany({})